package PT2019.demo.DemoProject;

import java.util.Comparator;

public class Monom implements Comparator<Monom>{
	int putere;
	int coef;
	boolean vizitat = false;
	
	
	public int compare(Monom arg0, Monom arg1) {
		return arg1.putere - arg0.putere; // sortare descrescatoare
	}
}
