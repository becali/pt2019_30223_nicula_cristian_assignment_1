

package PT2019.demo.DemoProject;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class App extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	JFrame frame = new JFrame();
	JPanel p = new JPanel();
	
	JButton bplus = new JButton("+");
	JButton bminus = new JButton("-");
	JButton bmul = new JButton("*");
	JButton bdiv = new JButton("/");
	JButton bderiv = new JButton("Derivare");
	JButton binteg = new JButton("Integrare");
	
	JTextField tf = new JTextField();
	JTextField tf2 = new JTextField(), tf_rez = new JTextField();
	
	JLabel pol = new JLabel("Introduceti polinoamele:");
	JLabel pol1 = new JLabel("Polinom 1");
	JLabel pol2 = new JLabel("Polinom 2");
	JLabel operatie = new JLabel("Selectati operatia dorita:");
	JLabel rez = new JLabel("Rezultat:");
	JLabel atentie = new JLabel("(!) Polinoamele vor fi introduse fara spatii, scriind puterea si pentru x^1, x^0.");
	JLabel asteRISC = new JLabel("(*) Derivarea si integrarea se calculeaza doar pentru polinomul 1.");
	JLabel bg = new JLabel();
	
	ImageIcon background = new ImageIcon("9343.jpg");

	
	
	void freim()
	{
		bplus.setBounds(25, 200, 85, 40);
		bminus.setBounds(115, 200, 85, 40);
		bmul.setBounds(205, 200, 85, 40);
		bdiv.setBounds(295, 200, 85, 40);
		bderiv.setBounds(385, 200, 85, 40);
		binteg.setBounds(475, 200, 95, 40);
		
		bg.setIcon(background);
		bg.setBounds(0,0,600,435);
			
		atentie.setFont(new Font("Arial", Font.PLAIN, 15));
		rez.setFont(new Font("Arial", Font.PLAIN, 20));
		pol.setFont(new Font("Arial", Font.PLAIN, 20));
		pol1.setFont(new Font("Arial", Font.PLAIN, 15));
		pol2.setFont(new Font("Arial", Font.PLAIN, 15));
		asteRISC.setFont(new Font("Arial", Font.PLAIN, 15));
		operatie.setFont(new Font("Arial", Font.PLAIN, 20));
		
		pol.setBounds(25, 10, 750, 40);
		pol1.setBounds(400, 50, 85, 40);
		pol2.setBounds(400, 100, 85, 40);
		operatie.setBounds(25, 150, 300, 40);
		rez.setBounds(25,250,200,40);
		atentie.setBounds(25,340,500,40);
		asteRISC.setBounds(25,365, 500, 40);
		
		tf.setFont(new Font("Arial", Font.PLAIN, 20));
		tf2.setFont(new Font("Arial", Font.PLAIN, 20));
		tf_rez.setFont(new Font("Arial", Font.PLAIN, 20));
		tf.setBounds(25, 50, 350, 40);
		tf2.setBounds(25, 100, 350, 40);
		tf_rez.setBounds(25, 290, 500, 40);
		tf_rez.setEditable(false);
		
		bplus.setFont(new Font("Arial", Font.BOLD, 20));
		bminus.setFont(new Font("Arial", Font.BOLD, 20));
		bmul.setFont(new Font("Arial", Font.BOLD, 20));
		bdiv.setFont(new Font("Arial", Font.BOLD, 20));
		binteg.setFont(new Font("Arial", Font.PLAIN, 15));
		bderiv.setFont(new Font("Arial", Font.PLAIN, 13));
	
		
		bplus.setBackground(new Color(217,217,217));
		bminus.setBackground(new Color(217,217,217));
		bmul.setBackground(new Color(217,217,217));
		bdiv.setBackground(new Color(217,217,217));
		binteg.setBackground(new Color(217,217,217));
		bderiv.setBackground(new Color(217,217,217));
		
		
		class ButtonListenerplus implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	//Operatii_polinoame op = new Operatii_polinoame();
	        	Polinom p1 = new Polinom(), p2 = new Polinom();
	        	Polinom p3 = new Polinom();
	        	p1 = Polinom.incapsulare_text(tf);
	        	p2 =  Polinom.incapsulare_text(tf2);
	        	Collections.sort(p1.poli, new Monom());
	        	Collections.sort(p2.poli, new Monom());
	        	p3=Operatii_polinoame.adunare(p1, p2);
	        	String afisare;
	        	afisare= Polinom.PolinomToString(p3);
	        	tf_rez.setText(afisare);
	        }
	    }
		
		class ButtonListenerminus implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	//Operatii_polinoame op = new Operatii_polinoame();
	        	Polinom p1 = new Polinom(), p2 = new Polinom();
	        	Polinom p3 = new Polinom();
	        	p1 =  Polinom.incapsulare_text(tf);
	        	p2 =  Polinom.incapsulare_text(tf2);
	        	Collections.sort(p1.poli, new Monom());
	        	Collections.sort(p2.poli, new Monom());
	        	p3=Operatii_polinoame.scadere(p1, p2);
	        	String afisare;
	        	afisare= Polinom.PolinomToString(p3);
	        	tf_rez.setText(afisare);
	        }
	    }
		
		class ButtonListenermul implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Polinom p1 = new Polinom(), p2 = new Polinom();
	        	Polinom p3 = new Polinom();
	        	p1 =  Polinom.incapsulare_text(tf);
	        	p2 =  Polinom.incapsulare_text(tf2);
	        	Collections.sort(p1.poli, new Monom());
	        	Collections.sort(p2.poli, new Monom());
	        	p3=Operatii_polinoame.inmultire(p1, p2);
	        	String afisare;
	        	afisare= Polinom.PolinomToString(p3);
	        	tf_rez.setText(afisare);
	        }
	    }
		
		class ButtonListenerimpartire implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Polinom p1 = new Polinom(), p2 = new Polinom();
	        	Impartit p3 = new Impartit();
	        	p1 =  Polinom.incapsulare_text(tf);
	        	p2 =  Polinom.incapsulare_text(tf2);
	        	Collections.sort(p1.poli, new Monom());
	        	Collections.sort(p2.poli, new Monom());
	        	p3=Operatii_polinoame.impartire(p1, p2,p3);
	        	String afisare="Cat: ";
	        	afisare=afisare.concat( Polinom.PolinomToString(p3.cat));
	        	afisare=afisare.concat("   Rest: ");
	        	afisare=afisare.concat( Polinom.PolinomToString(p3.rest));
	        	tf_rez.setText(afisare);
	        }
	    }
		
		class ButtonListenerderiv implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Polinom p1 = new Polinom(), p2 = new Polinom();
	        	p1 =  Polinom.incapsulare_text(tf);
	        	Collections.sort(p1.poli, new Monom());
	        	p2=Operatii_polinoame.derivare(p1);
	        	String afisare;
	        	afisare= Polinom.PolinomToString(p2);
	        	tf_rez.setText(afisare);
	        }
	    }
		
		class ButtonListenerinteg implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Polinom_double p1 = new Polinom_double(), p2 = new Polinom_double();
	        	p1 = Polinom_double.incapsulare_text(tf);
	        	Collections.sort(p1.poli, new Monom_double());
	        	p2=Operatii_polinoame.integrare(p1);
	        	String afisare;
	        	afisare=Polinom_double.PolinomToString(p2);
	        	tf_rez.setText(afisare);
	        }
	    }
		
		ButtonListenerplus pl = new ButtonListenerplus();
		ButtonListenerminus mi = new ButtonListenerminus();
		ButtonListenermul mu = new ButtonListenermul();
		ButtonListenerimpartire im = new ButtonListenerimpartire();
		ButtonListenerderiv de = new ButtonListenerderiv();
		ButtonListenerinteg in = new ButtonListenerinteg();
		
		bplus.addActionListener(pl);	
		bminus.addActionListener(mi);	
		bmul.addActionListener(mu);	
		bdiv.addActionListener(im);	
		bderiv.addActionListener(de);
		binteg.addActionListener(in);
		
		p.setLayout(null);
		p.add(asteRISC);
		p.add(atentie);
		p.add(rez);
		p.add(tf_rez);
		p.add(bplus);
		p.add(bminus);
		p.add(bmul);
		p.add(bdiv);
		p.add(bderiv);
		p.add(binteg);
		p.add(operatie);
		p.add(pol2);
		p.add(pol1);
		p.add(pol);
		p.add(tf);
		p.add(tf2);
		p.add(bg);
		frame.setSize(600, 460);
		frame.setContentPane(p);
		frame.setVisible(true);
	}
	public static void main( String[] args )
    {

		App da = new App();
		da.freim();  

    }
}
