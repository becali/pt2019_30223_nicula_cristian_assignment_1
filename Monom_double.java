package PT2019.demo.DemoProject;

import java.util.Comparator;

public class Monom_double implements Comparator<Monom_double>
{
	int putere;
	double coef;
	public int compare(Monom_double arg0, Monom_double arg1) {
		return arg1.putere - arg0.putere; // sortare descrescatoare
	}
}
