package PT2019.demo.DemoProject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JTextField;

public class Polinom
{
	List<Monom> poli = new ArrayList<Monom>(); 
	static Monom StringToMonom(String s)
	{
		Monom x = new Monom();
		if(s.startsWith("x") || s.startsWith("+x"))
			x.coef = 1;
		else
			if(s.startsWith("-x"))
				x.coef = -1;
			else
				 x.coef = Integer.parseInt(s.substring(0,s.indexOf('x')));
		x.putere = Integer.parseInt(s.substring(s.indexOf('^')+1,s.length()));
		return x;
	}
	
	static Polinom incapsulare_text(JTextField bla) // transforma textul intr un polinom, monom cu monom
	{
		String txt1;
		txt1 = bla.getText();
		Polinom p1 = new Polinom();
    	String mono = null;
    	int j;
    	while(txt1.isEmpty()==false)
    	{
	    	char c;
	    	j = txt1.indexOf("x")+2;
	    	if(j<txt1.length())
	    		c=txt1.charAt(j);
	    	else c = 'a';
	    	while (Character.isDigit(c)==true)
	    	{
	    		j++;
	    		if(j>=txt1.length())
	    			c='a';
	    		else
	    			c=txt1.charAt(j);
	    	}
	    	if(j>=txt1.length())
	    		j=txt1.length();
	    	mono = txt1.substring(0,j);
	    	Monom x = new Monom();
	    	x=StringToMonom(mono);	
	    	p1.poli.add(x);
	    	txt1=txt1.substring(j);
	    	j=0;
    	}		
    	return p1;
	}
	
	
	static String PolinomToString(Polinom p)
	{
		Collections.sort(p.poli, new Monom());
		String a="";		
		for(Monom x: p.poli)
		{
			if(x.coef!=0)
			{
				if(x.coef>0 && a.equals("")==false) // sa nu puna + in fata la primul monom
					a=a.concat("+");
				if(x.coef != 1)
					if(x.coef == -1)
						a=a.concat("-");
					else
						a=a.concat(String.valueOf(x.coef));
				if(x.putere!=0)
				{
					a=a.concat("x");
					if(x.putere != 1)
					{
						a=a.concat("^");
						a=a.concat(String.valueOf(x.putere));
					}
				}
				else if(x.putere==0)
				{
					if(x.coef==1 || x.coef==-1)
						a=a.concat("1");	
				}
			}
		}
		return a;
	}
}
