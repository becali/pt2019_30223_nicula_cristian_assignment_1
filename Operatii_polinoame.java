package PT2019.demo.DemoProject;


public class Operatii_polinoame {
	
	
	static Polinom adunare(Polinom a, Polinom b)
	{
		Polinom c = new Polinom(); // c = a + b
		if(b.poli.isEmpty()==false)
		{
			Monom suma = new Monom();
			for(Monom d :a.poli){
				int putere_b = b.poli.get(0).putere, i=1;
				int coef_b = b.poli.get(0).coef;
				while(d.putere != putere_b && i != b.poli.size()){
					putere_b = b.poli.get(i).putere;
					coef_b = b.poli.get(i).coef;
					i++;
				}
				if(i==b.poli.size() && d.putere != putere_b) // nu exista monom cu aceeasi putere in b
					suma = d;				
				else{
					Monom x = new Monom();
					x.coef=d.coef + coef_b;
					x.putere = d.putere;
					suma = x;
					b.poli.get(i-1).vizitat = true;
				}
				c.poli.add(suma);
			}
			for(Monom x: b.poli) // termenii care nu au pereche in celalalt polinom se adauga la final
				if(x.vizitat == false)
					c.poli.add(x);
		}	
		return c;
	}
	
	static Polinom scadere(Polinom a, Polinom b)
	{	
		Polinom c = new Polinom();
		int i = 0;
		while(i<b.poli.size())
		{
			b.poli.get(i).coef *= -1;
			i++;
		}
		c=adunare(a,b);
		return c;
	}
	
	// inmultesc fiecare termen din polinomul a cu tot polinomul b
	// apoi adun polinoamele rezultate
	
	static Polinom inmultire(Polinom a, Polinom b)
	{
		Polinom produs = new Polinom();
		for(Monom i_a: a.poli)
		{
			Polinom termen = new Polinom();
			for(Monom i_b: b.poli)
			{
				Monom p = new Monom();
				p.putere = i_a.putere + i_b.putere;
				p.coef = i_a.coef * i_b.coef;
				termen.poli.add(p);
			}
			produs=adunare(produs,termen);			
		}
		return produs;
	}
	
	static boolean mai_mare(Polinom a, Polinom b) // 1 daca a e mai mare, -1 daca nu
	{
		int i=0, j=0;

		
		while(i<a.poli.size())
		{
			while(a.poli.get(i).coef == 0)
				i++;
			while(b.poli.get(j).coef == 0)
				j++;
			if(a.poli.get(i).putere > b.poli.get(j).putere)
				return true;
			if(a.poli.get(i).putere == b.poli.get(j).putere)
				if(Math.abs(a.poli.get(i).coef) > Math.abs(b.poli.get(j).coef))
					return true;
			i++;
		}
		return false;
	}
	
	//static Impartit rezultat = new Impartit();

	static Impartit impartire(Polinom a, Polinom b, Impartit rezultat)  
	{
		Polinom rest = new Polinom(), inmult = new Polinom(),aux_a = a;Polinom aux_b = b;
		Monom imp = new Monom(); // impartim primul termen al deimpartitului la primul termen al impartitorului 
		int i=0, j=0;
		while(aux_a.poli.get(i).coef==0)
			i++;
		while(aux_b.poli.get(j).coef==0)
			j++;
		if(j == b.poli.size()) // daca impartim la zero
			return null;
		if(i == a.poli.size())
			return rezultat;
		imp.coef = aux_a.poli.get(i).coef / aux_b.poli.get(j).coef;
		imp.putere = aux_a.poli.get(i).putere - aux_b.poli.get(j).putere;
		rezultat.cat.poli.add(imp);
		
		Polinom el_cat = new Polinom(); // pentru a putea inmulti un polinom cu un monom
		el_cat.poli.add(imp);
		inmult = inmultire(el_cat,b);
		rest = scadere(a,inmult);
		rezultat.rest = rest;
		
		// daca catul e mai mic decat restul continuam impartirea
		if(mai_mare(b,rest)==false) 
		{
			impartire(rest,b,rezultat); //scadem restul din deimpartit si reluam impartirea
		}
		return rezultat;	
		
	}
	
	static Polinom derivare(Polinom a)
	{
		Polinom d = new Polinom();
		for(Monom x : a.poli)
		{
			Monom mono = new Monom();
			mono.coef = x.coef*x.putere;
			mono.putere = x.putere-1;
			d.poli.add(mono);
		}
		return d;
	}
	
	static Polinom_double integrare(Polinom_double a)
	{
		Polinom_double d = new Polinom_double();
		for(Monom_double x : a.poli)
		{
			Monom_double mono = new Monom_double();
			mono.coef = x.coef/(x.putere+1);
			mono.putere = x.putere+1;
			d.poli.add(mono);
		}
		return d;
	}
}
