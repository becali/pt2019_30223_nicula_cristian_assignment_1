package PT2019.demo.DemoProject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class JunitTests   {

	Polinom genereaza_a()
	{
		Polinom a = new Polinom();
		int i=1;
		while(i<5)
		{
			Monom aux = new Monom();
			aux.coef=7-i;
			aux.putere=5-i;
			a.poli.add(aux);
			i++;
		}
		return a;
	}
	
	Polinom genereaza_b()
	{
		Polinom a = new Polinom();
		int i=1;
		while(i<5)
		{
			Monom aux = new Monom();
			aux.coef=5-i;
			aux.putere=6-i;
			a.poli.add(aux);
			i++;
		}
		return a;
	}
	
	@Test
	public void testAdunare() {
		Polinom a, b, c = new Polinom();
		Polinom expected = new Polinom();
		a = genereaza_a();
		b = genereaza_b();
		int i;
		// a = 6x^4+5x^3+4x^2+3x
		// b = 4x^5+3x^4+2x^3+x^2
		// expected: 9x^4+7x^3+5x^2+3x+4x^5		
		expected.poli.add(Polinom.StringToMonom("9x^4"));
		expected.poli.add(Polinom.StringToMonom("7x^3"));
		expected.poli.add(Polinom.StringToMonom("5x^2"));
		expected.poli.add(Polinom.StringToMonom("3x^1"));
		expected.poli.add(Polinom.StringToMonom("4x^5"));
		c = Operatii_polinoame.adunare(a, b);
		for(i=0;i<5;i++)
		{
			assertEquals(c.poli.get(i).coef,expected.poli.get(i).coef);
			assertEquals(c.poli.get(i).putere,expected.poli.get(i).putere);
		}
	}
	
	@Test
	public void testScadere() {
		Polinom a, b, c = new Polinom();
		Polinom expected = new Polinom();
		a = genereaza_a();
		b = genereaza_b();
		int i;
		// a = 6x^4+5x^3+4x^2+3x
		// b = 4x^5+3x^4+2x^3+x^2
		// expected: -4x^5+3x^4+3x^3+3x^2+3x
		
		expected.poli.add(Polinom.StringToMonom("3x^4"));
		expected.poli.add(Polinom.StringToMonom("3x^3"));
		expected.poli.add(Polinom.StringToMonom("3x^2"));
		expected.poli.add(Polinom.StringToMonom("3x^1"));
		expected.poli.add(Polinom.StringToMonom("-4x^5"));
		c = Operatii_polinoame.scadere(a, b);
		for(i=0;i<5;i++)
		{
			assertEquals(c.poli.get(i).coef,expected.poli.get(i).coef);
			assertEquals(c.poli.get(i).putere,expected.poli.get(i).putere);
		}
	}
	
	@Test
	public void testMul() {
		Polinom a, b, c = new Polinom();
		Polinom expected = new Polinom();
		a = genereaza_a();
		b = genereaza_b();
		int i;
		// a = 6x^4+5x^3+4x^2+3x
		// b = 4x^5+3x^4+2x^3+x^2
		
		expected.poli.add(Polinom.StringToMonom("24x^9"));
		expected.poli.add(Polinom.StringToMonom("38x^8"));
		expected.poli.add(Polinom.StringToMonom("43x^7"));
		expected.poli.add(Polinom.StringToMonom("40x^6"));
		expected.poli.add(Polinom.StringToMonom("22x^5"));
		expected.poli.add(Polinom.StringToMonom("10x^4"));
		expected.poli.add(Polinom.StringToMonom("3x^3"));
		c = Operatii_polinoame.inmultire(a, b);
		for(i=0;i<7;i++)
		{
			assertEquals(c.poli.get(i).coef,expected.poli.get(i).coef);
			assertEquals(c.poli.get(i).putere,expected.poli.get(i).putere);
		}
	}
	
	@Test
	public void testDeriv() {
		Polinom a, c = new Polinom();
		Polinom expected = new Polinom();
		a = genereaza_a();
		
		int i;
		// a = 6x^4+5x^3+4x^2+3x
			
		expected.poli.add(Polinom.StringToMonom("24x^3"));
		expected.poli.add(Polinom.StringToMonom("15x^2"));
		expected.poli.add(Polinom.StringToMonom("8x^1"));
		expected.poli.add(Polinom.StringToMonom("3x^0"));
	
		c = Operatii_polinoame.derivare(a);
		for(i=0;i<4;i++)
		{
			assertEquals(c.poli.get(i).coef,expected.poli.get(i).coef);
			assertEquals(c.poli.get(i).putere,expected.poli.get(i).putere);
		}
	}
	
	Polinom_double genereaza_c()
	{
		Polinom_double c = new Polinom_double();
		int i=5;
		while(i!=1)
		{
			Monom_double aux = new Monom_double();
			aux.coef=i;
			aux.putere=i-1;
			c.poli.add(aux);
			i--;
		}
		return c;
	}
	
	@Test
	public void testInteg() {
		Polinom_double a, c = new Polinom_double();
		Polinom_double expected = new Polinom_double();
		a = genereaza_c();
		
		int i;
		// a = 5x^4+4x^3+3x^2+2x^1
			
		expected.poli.add(Polinom_double.StringToMonom("x^5"));
		expected.poli.add(Polinom_double.StringToMonom("x^4"));
		expected.poli.add(Polinom_double.StringToMonom("x^3"));
		expected.poli.add(Polinom_double.StringToMonom("x^2"));
	
		c = Operatii_polinoame.integrare(a);
		for(i=0;i<4;i++)
		{
			assertEquals(c.poli.get(i).putere,expected.poli.get(i).putere);
		}
	}
}
